#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Top Block
# Generated: Mon Apr 13 12:11:26 2015
##################################################

from gnuradio import audio
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import window
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import scopesink2
from gnuradio.wxgui import waterfallsink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class top_block(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Top Block")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.samp_rate = samp_rate = 44000
		self.prf = prf = 10*10
		self.n_shift = n_shift = 5*10
		self.n_repeat = n_repeat = 10
		self.bw = bw = 10*1000

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "B-Scan")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Range Spectrum")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Waveform")
		self.Add(self.notebook_0)
		self.wxgui_waterfallsink2_0 = waterfallsink2.waterfall_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			baseband_freq=0,
			dynamic_range=100,
			ref_level=0,
			ref_scale=2.0,
			sample_rate=samp_rate,
			fft_size=1024,
			fft_rate=40,
			average=False,
			avg_alpha=None,
			title="B-Scan Image",
			win=window.rectangular,
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_waterfallsink2_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(2).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_scopesink2_0.win)
		self.wxgui_fftsink2_0 = fftsink2.fft_sink_f(
			self.notebook_0.GetPage(1).GetWin(),
			baseband_freq=0,
			y_per_div=10,
			y_divs=10,
			ref_level=0,
			ref_scale=2.0,
			sample_rate=samp_rate,
			fft_size=1024,
			fft_rate=15,
			average=False,
			avg_alpha=None,
			title="FFT Plot",
			peak_hold=False,
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_fftsink2_0.win)
		self.low_pass_filter_0 = gr.fir_filter_fff(1, firdes.low_pass(
			1, samp_rate/2, 0.95*bw, 10, firdes.WIN_BLACKMAN, 6.76))
		self.gr_vco_f_0 = gr.vco_f(samp_rate, (2*3.14)*bw, 1)
		self.gr_sig_source_x_0 = gr.sig_source_f(samp_rate, gr.GR_SAW_WAVE, prf, 1, 0)
		self.gr_multiply_xx_0_0 = gr.multiply_vff(1)
		self.gr_multiply_const_vxx_0 = gr.multiply_const_vff((1000, ))
		self.gr_float_to_complex_0 = gr.float_to_complex(1)
		self.audio_source_0 = audio.source(samp_rate, "", True)
		self.audio_sink_0 = audio.sink(samp_rate, "", True)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_float_to_complex_0, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_vco_f_0, 0))
		self.connect((self.gr_vco_f_0, 0), (self.audio_sink_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_float_to_complex_0, 0))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.low_pass_filter_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.wxgui_fftsink2_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.wxgui_waterfallsink2_0, 0))
		self.connect((self.audio_source_0, 0), (self.gr_multiply_const_vxx_0, 0))
		self.connect((self.gr_multiply_const_vxx_0, 0), (self.gr_float_to_complex_0, 1))
		self.connect((self.gr_vco_f_0, 0), (self.gr_multiply_xx_0_0, 0))
		self.connect((self.gr_multiply_const_vxx_0, 0), (self.gr_multiply_xx_0_0, 1))


	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.wxgui_waterfallsink2_0.set_sample_rate(self.samp_rate)
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate/2, 0.95*self.bw, 10, firdes.WIN_BLACKMAN, 6.76))

	def get_prf(self):
		return self.prf

	def set_prf(self, prf):
		self.prf = prf
		self.gr_sig_source_x_0.set_frequency(self.prf)

	def get_n_shift(self):
		return self.n_shift

	def set_n_shift(self, n_shift):
		self.n_shift = n_shift

	def get_n_repeat(self):
		return self.n_repeat

	def set_n_repeat(self, n_repeat):
		self.n_repeat = n_repeat

	def get_bw(self):
		return self.bw

	def set_bw(self, bw):
		self.bw = bw
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate/2, 0.95*self.bw, 10, firdes.WIN_BLACKMAN, 6.76))

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = top_block()
	tb.Run(True)

