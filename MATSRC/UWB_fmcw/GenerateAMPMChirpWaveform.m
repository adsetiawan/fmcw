% ------------------------------------------------------------------
--------- %
% Generate AMPM Chirp Waveform
% ========================
%
% Returns :
% ---------
% Waveform -> Chirp Waveform (time-domain).
%
% ------------------------------------------------------------------
--------- %
function [Waveform] = GenerateAMPMChirpWaveform(radarParams,PMPoly,AMDCT)
% Load Paramters.
nT = radarParams(2); % # Time Points.
tS = radarParams(3); % Time Step.
t0 = radarParams(4); % Time Offset (not user in Ver 1.0).
f0 = radarParams(5); % Initial Frequency.
tD_act = radarParams(6); % Time Duration (not used in Ver 1.0).
tD_sim = nT*tS; % .. Set to nT x tS.
f1 = radarParams(7); % Final Frequency.
fc = (f0 + f1)/2; % Center Frequency.
B = (f1 - f0); % 3 dB bandwidth.
% Generate a time Scale.
t = (0:tS:(nT-1)*tS)'; % Time Vector.
% Generate the Phase
PMPoly_Sim = PMPoly.*(tD_act/tD_sim).^(length(PMPoly)-2:-1:-1);
Phse = polyval(PMPoly_Sim,t);
% Debug
% k = B/(nT*tS); % Sweep Rate.
% Phse1 = (pi*k*t.^2+2*pi*f0*t);
% plot(t,Phse,t,Phse1);
% Generate the Waveform
Waveform = real(exp(j*Phse));
Waveform =(1+idct(AMDCT,length(Waveform))).*Waveform;