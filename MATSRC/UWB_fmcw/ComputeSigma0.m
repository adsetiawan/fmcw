function [Sigma0,Distance] = ComputeSigma0(Freq, Ht, Fs, R_Res, ...
Sig_h_1, Sig_h_2, L_c_1, L_c_2,geoData)
addpath('../Library/PermCalc');
% Omni-Present Definitions !!
Epsilon0 = 1e-9/(36*pi);
Mu0 = (4e-7)*pi;
Angle_Rad = [0:5:60]'*pi/180;
Angle_Rad_2s = [-60:5:60]'*pi/180;
% To Apply Antenna beam pattern
Gain_2s = gausswin(length(Angle_Rad_2s),7.17); % Coeff adjusted to
get 10deg BmW
% Range from Angle
Layer_Ht = sum(geoData(2:end-1,1)); % Exclude Air and Sea Ice to get
snow height.
Ranges_1 = Ht./cos(Angle_Rad);
75
Cell_Num_1 = floor((Ranges_1 - Ht)/R_Res);
Ranges_2 = (Ht+Layer_Ht)./cos(Angle_Rad);
Cell_Num_2 = floor((Ranges_2 - Ht - Layer_Ht)/R_Res);
% Define
Dens_Ic = 918; % Ice Densit
Er_Air = 1;
Er_Ice = 3.15 - 0.001j;
K = (Er_Ice - Er_Air)/(Er_Ice + Er_Air); %Constant
% Compute WvNum
WvLen = 3e8/Freq;
WvNum = 2*pi./WvLen;
% Extract Snow Info
Temp_Sn = geoData(2,3);
Dens_Sn = geoData(2,4);
Radi_Sn = 0.5e-3; %% Assume Now, Later include in geoData.
Er_Layer = ComputeDrySnowEr(Dens_Sn,Temp_Sn,Freq);
% Number of Scatterers
Vol_Frct = Dens_Sn / Dens_Ic;
Vol_Snow = 4*pi*Radi_Sn^3/3;
Num_Scat = Vol_Frct/Vol_Snow;
Eq_Radi_Sn = (1.2+Vol_Frct-2*Vol_Frct^2)*Radi_Sn;
% Extract Sea Ice Info
Temp_SI = geoData(end,3);
Sali_SI = geoData(end,4);
Dens_SI = geoData(end,5);
% Mie Parameter
Chi = 2*pi*Radi_Sn*sqrt(Er_Air)./WvLen;
% Initialize Return Variable 1
Distance = [min(Ranges_1):0.01:max(Ranges_2)];
VScatTerm_1 = zeros(size(Angle_Rad));
VScatTerm_2 = zeros(size(Angle_Rad));
Area_1 = zeros(size(Angle_Rad));
Area_2 = zeros(size(Angle_Rad));
Sigma_0_Sn = zeros(size(Angle_Rad));
Sigma_0_Ic = zeros(size(Angle_Rad));
Sigma_0_ss = zeros(size(Angle_Rad));
Sigma_0_sv = zeros(size(Angle_Rad));
Sigma_0_is = zeros(size(Angle_Rad));
% Some Calcs out of the Loop
Sigma_B = 8*pi*WvNum^4*Eq_Radi_Sn^6*abs(K)^2/3;
Sigma_V = Num_Scat * Sigma_B;
Qs = 2*WvLen^2*Chi^6*abs(K)^2/(3*pi);
Qa = WvLen^2*Chi^3*imag(-K)/(pi);
76
ks = Num_Scat * Qs;
kai = Num_Scat * Qa;
kab = 2*WvNum*(1-Vol_Frct)*imag(sqrt(Er_Air));
ka = kai + kab;
ke = ks + ka;
for index_var = 1:length(Angle_Rad)
Sigma_0_ss(index_var) =
ReturnScatterCoeff(Angle_Rad(index_var),Freq,Sig_h_1,L_c_1,1,Er_Laye
r);
Angle_Vol =
real(asin(sin(Angle_Rad(index_var))/sqrt(Er_Layer)));
Er_Final =
ComputeSeaIceEr(Dens_SI,Sali_SI,Temp_SI,rad2deg(Angle_Vol),Freq,1);
Loss = exp(ke*Layer_Ht*sec(Angle_Vol));
Sigma_0_sv(index_var) = Sigma_V * cos(Angle_Vol) * (1 -
1/Loss^2) / (2*ke);
Refl = (sqrt(Er_Air)*cos(Angle_Rad(index_var))-
sqrt(Er_Layer)*cos(Angle_Vol))/...
(sqrt(Er_Air)*cos(Angle_Rad(index_var))+sqrt(Er_Layer)*cos(Angle_Vol
));
Transm_s = 1-abs(Refl)^2;
Sigma_0_is(index_var) =
ReturnScatterCoeff(Angle_Vol,Freq,Sig_h_2,L_c_2,Er_Layer,Er_Final);
Sigma_0_Sn(index_var) = Sigma_0_ss(index_var) +
Transm_s*(Sigma_0_sv(index_var));
Sigma_0_Ic(index_var) = Transm_s*Sigma_0_is(index_var)/Loss;
if (Cell_Num_1(index_var) == 0)
Radius_2 = (Ht + R_Res)^2 - Ht^2;
Area_1(index_var) = pi * Radius_2;
VScatTerm_1(index_var) =
Sigma_0_Sn(index_var)*Area_1(index_var)/((4*pi)^3*Ranges_1(index_var
)^4);
else
Radius_1_2 = (Ht + Cell_Num_1(index_var)*R_Res)^2 - Ht^2;
Radius_2_2 = (Ht + (Cell_Num_1(index_var) + 1)*R_Res)^2 -
Ht^2;
Area_1(index_var) = pi * (Radius_2_2 - Radius_1_2);
VScatTerm_1(index_var) =
Sigma_0_Sn(index_var)*Area_1(index_var)/((4*pi)^3*Ranges_1(index_var
)^4);
end;
77
if (Cell_Num_2(index_var) == 0)
Radius_2 = (Ht+Layer_Ht + R_Res)^2 - Ht^2;
Area_2(index_var) = pi * Radius_2;
VScatTerm_2(index_var) =
Sigma_0_Ic(index_var)*Area_2(index_var)/((4*pi)^3*Ranges_2(index_var
)^4);
else
Radius_1_2 = (Ht+Layer_Ht + Cell_Num_2(index_var)*R_Res)^2
- Ht^2;
Radius_2_2 = (Ht+Layer_Ht + (Cell_Num_2(index_var) +
1)*R_Res)^2 - Ht^2;
Area_2(index_var) = pi * (Radius_2_2 - Radius_1_2);
VScatTerm_2(index_var) =
Sigma_0_Ic(index_var)*Area_2(index_var)/((4*pi)^3*Ranges_2(index_var
)^4);
end;
end;
% figure;
% plot(Angle_Rad*180/pi,10*log10(Sigma_0_ss),'g*-
',Angle_Rad*180/pi,10*log10(Sigma_0_sv),'rs-
',Angle_Rad*180/pi,10*log10(Sigma_0_is),'cd-');
% title('Scattering Coefficient vs. Angle');
% xlabel('\theta');
% ylabel('\sigma^0');
% legend('\sigma^0_s_s','\sigma^0_s_v','\sigma^0_i_s');
% grid;
VScatTerm_2s_1 = [flipud(VScatTerm_1(2:end));VScatTerm_1];
VScatTerm_2s_2 = [flipud(VScatTerm_2(2:end));VScatTerm_2];
% Convolve with gain
VScatTerm_Gain_1 = flipud(fftfilt(VScatTerm_2s_1,Gain_2s));
VScatTerm_Gain_2 = flipud(fftfilt(VScatTerm_2s_2,Gain_2s));
VScatTerm_Gain_1 = VScatTerm_Gain_1(1:length(Angle_Rad));
VScatTerm_Gain_2 = VScatTerm_Gain_2(1:length(Angle_Rad));
%% % Normalization : Match Maxmimum After Gain Convolution!
%% VScatTerm_Gain_1 =
VScatTerm_Gain_1/max(VScatTerm_Gain_1)*max(VScatTerm_2s_1);
%% VScatTerm_Gain_2 =
VScatTerm_Gain_2/max(VScatTerm_Gain_2)*max(VScatTerm_2s_2);
VScatTerm_Gain =
interp1(Ranges_1,VScatTerm_Gain_1,Distance,'',0)+...
interp1(Ranges_2,VScatTerm_Gain_2,Distance,'',0);
% Include Lamda Square Term
VScatTerm = VScatTerm_Gain.*WvLen^2;
Sigma0 = sqrt(VScatTerm);
78
return;