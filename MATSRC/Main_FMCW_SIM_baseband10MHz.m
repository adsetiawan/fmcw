% ==================================================
% FMCW-radar simulation
% baseband, designed for GRC simulation
% Created by: Andriyan B. Suksmono
%  School of Elec. Eng. & Informatics/IRCTR-IB
%  Institut Teknologi Bandung, Indonesia
% Date: April/2013
% ==================================================
% Natural constants
c=3e8; %the speed of light

% operational parameters
sens_VCO =628E5; % VCO sensitivity
max_SAW =1; % maximum sawtooth amplitude

BW = sens_VCO*max_SAW/(6.28) ; % radar bandwidth ;
% GNU radio sampling rate
samp_rate=100E6; % 10 Mega samples/s
dt=1/samp_rate;
% range cell number
N_cell = 512; %1024; N-fft ?
% range resolution
cell_res = c/(2*BW);
% maximum unambiguous range
max_range = N_cell*cell_res; % = 5120 meters
% sweep time = T = max_range / speed-of-light
T = max_range/c;
% FM rate K = Bandwidth/sweep_time
K=BW/T;
% discretization of time; dt = delay_time_of_max_range/(2*N_cell)

%dt=1/samp_rate;%T/(2*N_cell);
t=0:dt:T;

%generate chirp
s_chirp = sin(pi*K*(t.*t));

% ==================================================
% Baseband Processing Only, no carrier
% ==================================================
%generate transmitted chirp
s=s_chirp; 
figure(1);plot(t,s);
xlabel('time (s)'); ylabel('Amplitude (V)');
title('Chirp');

% plot spectrum of s
S=fft(s); 
S_MAG=abs(S);
df=1/T;
f=(0:df:2*BW-df);
figure(2); 
plot(f,S_MAG(1:floor(N_cell)));
title('Chirp spectrum');
xlabel('Frequency (Hz)'); ylabel('Magnitude');
%loglog(S_MAG(1:floor(N_cell)));

% simulate reflection
% position of target: 0.25*maxrange ;
L= 0.25*max_range;
tau=2*L/c;
n_delay=tau/dt;
%received chirp
r=sin(pi*K*((t-tau).*(t-tau)));
figure(3);plot(t,r); 
title('Received chirp');
xlabel('Time (s)'); ylabel('Amplitude');

%Create range-axis
x=cell_res*(0:N_cell-1);
%plot spectrum of r
R=fft(r); 
R_MAG=abs(R);
figure(10);
plot(x,R_MAG(1:floor(N_cell)));
title('Range spectrum');
xlabel('Range (m)'); ylabel('Magnitude');
%loglog(R_MAG(1:floor(N_cell)));
% 
%demodulate,
dem=r.*s;
% no-carrier -> no filtering -> get demodulated signal
% involve filtering
% BW of beat_signal = BW_radar = max_allowed frequency
f = [0 0.6 0.6 1]; m = [1 1 0 0];
b = fir2(30,f,m);
% 
f_beat=filter(b,1,dem);
figure(11);
subplot(211);plot(dem); title('beat signal unfiltered');
subplot(212);plot(f_beat);title('filtered beat');
% display range-spectrum
F_BEAT=fft(f_beat);
F_BEAT_MAG=abs(F_BEAT);
%create range-axis
figure(20);
plot(x,F_BEAT_MAG(1:floor(N_cell)));
%plot(x,real(F_BEAT(1:floor(N_cell))));
xlabel('Range (m)'); ylabel('Magnitude');
title('Range Spectrum of Object at 3000 m');
