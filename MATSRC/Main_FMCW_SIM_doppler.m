% ==================================================
% FMCW-radar simulation with moving object
% Created by: Andriyan B. Suksmono
%  School of Elec. Eng. & Informatics/IRCTR-IB
%  Institut Teknologi Bandung, Indonesia
% Date: 26/July/2010
% ==================================================
% Natural constants
c=3e8; %the speed of light
% operational parameters
fc = 2.8E9; % the radar works at 2.8 GHz 
BW = 30E6 ; % radar bandwidth is 30 MHz;
% range cell number
N_cell = 1024; 
% range resolution
cell_res = c/(2*BW);
% maximum unambiguous range
max_range = N_cell*cell_res; % = 5120 meters
% sweep time = T = max_range / speed-of-light
T = max_range/c;
% FM rate K = Bandwidth/sweep_time
K=BW/T;
% discretization of time; dt = delay_time_of_max_range/(2*N_cell)

dt=T/(2*N_cell);
t=0:dt:T-dt;

%generate chirp
s_chirp = sin(pi*K*(t.*t));

% ==================================================
% Baseband Processing Only, no carrier
% ==================================================
%generate transmitted chirp
s=s_chirp; 
figure(1);plot(t,s);
xlabel('time (s)'); ylabel('Amplitude (V)');
title('Chirp');

% plot spectrum of s
S=fft(s); 
S_MAG=abs(S);
df=1/T;
f=(0:df:2*BW-df);
figure(2); 
plot(f,S_MAG(1:floor(N_cell)));
title('Chirp spectrum');
xlabel('Frequency (Hz)'); ylabel('Magnitude');
%loglog(S_MAG(1:floor(N_cell)));

% simulate reflection
% position of target: 3Km ;

L= 3000;
tau=2*L/c;

% Doppler shift of object moves with relative speed v
% is df = (v/c)f = (v/lambda)
% Define an object moves with radial speed = 100 m/s
% v=100; % m/s
% % beat frequency is = 2v/(lambda) + (2R0/c) K;
% 
% f_beatdopp = (2*v/c) + (2*L/c)*K;
% r1 = sin(f_beatdopp*(t-tau));
% figure(1000);plot(r1);

%received chirp
r=sin(pi*K*((t-tau).*(t-tau)));
figure(3);plot(t,r); 
title('Received chirp');
xlabel('Time (s)'); ylabel('Amplitude');

% create an array of observation
% assume that the object moves one range-cell per-observation
% resolution = cell_res => dtau=cell_res/c;

Nsweep=64;
r_array=zeros(Nsweep,2*N_cell);
dtau=cell_res/c;

for m=1:Nsweep;
    r_array(m,:) = sin(pi*K*((t-tau-m*dtau).*(t-tau-m*dtau)))+r;
end;

figure(50);plot(t,r_array); 
title('Received chirp');
xlabel('Time (s)'); ylabel('Amplitude');

    
%Create range-axis
x=cell_res*(0:N_cell-1);
%plot spectrum of r
R_ARRAY=fft(r_array); 
R_MAG_ARRAY=abs(R_ARRAY);

% figure(51);
% plot(x,R_MAG_ARRAY(1:floor(N_cell)));
% title('Range spectrum');
% xlabel('Range (m)'); ylabel('Magnitude');
% %loglog(R_MAG(1:floor(N_cell)));
% 
%demodulate

dem_array=zeros(Nsweep,2*N_cell);
for m=1:Nsweep;
    dem_array(m,:)=r_array(m,:).*s;
end;

% dem=r.*s;
% no-carrier -> no filtering -> get demodulated signal
f_beat_array=dem_array;
%figure(51);plot(f_beat_array); title('beat signal unfiltered');

% display range-spectrum
F_BEAT_ARRAY=zeros(Nsweep,2*N_cell);;
for m=1:Nsweep;
    F_BEAT_ARRAY(m,:)=fft(f_beat_array(m,:));
end;
F_BEAT_MAG_ARRAY=abs(F_BEAT_ARRAY);
%create range-axis
figure(52);
%plot(x,F_BEAT_MAG_ARRAY(:, 1:floor(N_cell)));
imagesc(F_BEAT_MAG_ARRAY(:, 1:floor(N_cell)));
%plot(x,real(F_BEAT(1:floor(N_cell))));
xlabel('Range (m)'); ylabel('Magnitude');
title('Range Spectrum of Object at 3000 m');
%Doppler processing
S_DOPP=zeros(2*N_cell,Nsweep);;

for m=1:N_cell;
    S_DOPP(m,:)=fftshift(F_BEAT_MAG_ARRAY(:,m));
    %S_DOPP(m,:)=fftshift(F_BEAT_ARRAY(:,m));
    %S_DOPP(m,:)=fft(f_beat_array(:,m));
end;

figure(53);
%imagesc(abs(S_DOPP(1:N_cell,1:floor(Nsweep/2)))'); title('doppler spectrum');    
imagesc(abs(S_DOPP)'); title('doppler spectrum');    

% NEXT:
% -> multiple reflections
% -> carrier and noise
% -> match filtering 
% -> Doppler processing ?


% %SFCW-like processing
% sin_chirp = sin(pi*K*(t.*t));
% cos_chirp = cos(pi*K*(t.*t));
% 
% %I/Q demodulation with the chirp and its quadrature
% I=r.*sin_chirp;
% Q=r.*cos_chirp;
% 
% %
% g=ifft(I+i*Q);
% %g=fft(sin_chirp+i*cos_chirp);
% figure(31);plot(x,real(g(1:N_cell)));
% xlabel('Range (m)'); ylabel('Amplitude');
% grid on;
% 
% figure(32);plot(x,abs(g(1:N_cell)));
% xlabel('Range (m)'); ylabel('Amplitude');
% grid on;
% 
% %figure(31);plot(x,abs(g(1:N_cell)));
%---